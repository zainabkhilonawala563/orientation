### **What is Git?**
Git is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows.

### **Important terms to know**
1. Repository\
It is a collection of files and folders that helps you and your team to track the project's progress and changes made in the code.

2. Branch\
Branches are separate instances of the code that are different from the main code database.\
**$ git checkout master**\
**$ git checkout -b <your-branch-name>**\
The above commands are used to create branches.\


3. Forking\
Forking a github repository creates a copy of that repository in your account. This helps developers in editing the code in their forked repository without impacting the parent repository.\
![alt text](https://www.earthdatascience.org/images/earth-analytics/git-version-control/git-fork-clone-flow.png)

4. Cloning\
**$ git clone <link-to-repository>**\
The above command is used to clone a repository. Cloning a repository means creating your own copy of the repositoryon your local machine.

5. Commit\
**git commit -sv**\
Using this command, one has save his/her work on their local repository.\


6. Merge\
The git merge command lets you take the independent lines of development created by git branch and integrate them into a single branch.\
![alt text](https://res.cloudinary.com/practicaldev/image/fetch/s--MEKaM3dY--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://cl.ly/430Q2w473e2R/Image%25202018-04-30%2520at%25201.07.58%2520PM.png)

7. Push command \
**$ git push origin <branch-name>**
The git push command is used to upload local repository content to a remote repository. By using the above command, we can accomplish the same.

**To add changes to the staging area the following command is used:**
**$ git add .** 

### **Installing Git**
The following command can be used to install Git on your local machine having a Microsoft, Linux or Mac OS.\
**sudo apt-get install git** 

### **Stages that occur in a Git Workflow**
![alt text](https://i.imgur.com/oodiCnB.png)
