### **What is Docker?**
Docker is a set of platform as a service products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels.
![alt text](https://www.cloudsavvyit.com/thumbcache/0/0/576bdb68675c300a17d82c131b23c927/p/uploads/2019/06/c454d054.png)

### **What is a Docker Image?**
A Docker image is a read-only template that contains a set of instructions for creating a container that can run on the Docker platform. It provides a convenient way to package up applications and preconfigured server environments, which you can use for your own private use or share publicly with other Docker users./
A docker image contains all the resources required to run an application as a container such as code, runtime, libraries, environment variables, configuration files.\
![alt text](https://www.docker.com/sites/default/files/d8/styles/large/public/2018-11/container-what-is-container.png?itok=vle7kjDj)

### **Docker Architecture**
![alt text](https://www.whizlabs.com/blog/wp-content/uploads/2019/08/Docker_Architecture.png)

### **Docker Containers**
![alt text](https://www.studytrails.com/wp-content/uploads/2019/04/containers_intro-1024x576.jpg)

### **Docker Hub**
Docker Hub is like GitHub but for docker images and containers.\
![alt text](https://miro.medium.com/max/826/1*ttU6oMoZztKk2kjJid6PuQ.png)

### **Docker Installation**
1. **Uninstall the older version of docker if is already installed.**

$ sudo apt-get remove docker docker-engine docker.io containerd runc

2. **Installing CE (Community Docker Engine)**

$ sudo apt-get update
$ sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common \
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - \
$ sudo apt-key fingerprint 0EBFCD88 \
$ sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable nightly test" \
$ sudo apt-get update\
$ sudo apt-get install docker-ce docker-ce-cli containerd.io 

// Check if docker is successfully installed in your system \
$ sudo docker run hello-world

### **Docker Basic Commands**
![alt text](https://raw.githubusercontent.com/sangam14/dockercheatsheets/master/dockercheatsheet1.png)
![alt text](https://4.bp.blogspot.com/-kucMY32s0OQ/WRfbTpTzTsI/AAAAAAAAA3Q/xzSbNomHOCw8NyMPvBj0orQ2X2C3JJE8gCLcB/s1600/flowchart.png) 

### **Common Operations on Dockers**
1. Download the docker.\
docker pull abc/trydock 
2. Run the docker image with this command.\
docker run -ti abc/trydock /bin/bash 

3. Copying code inside docker.
* Create a .py file with some code and save the .py file. 
* Copy file inside the docker container. 

docker cp firstprogram.py e0b72ff850f8: \
where e0b72ff850f8 is the containerID. This will copy firstprogram.py inside docker in root directory.

* All write a script for installing dependencies - requirements.sh \

apt update \
apt install python3 
* Copy file inside docker. \
docker cp requirements.sh e0b72ff850f8:/ 

4. Install dependencies \
 If you want to install additional dependencies then you can, write all the steps for installation in a shell script and run the script inside the    container. 

* Give permission to run shell script. \
docker exec -it e0b72ff850f8 chmod +x requirements.sh 
* Install dependencies. \
docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh 
5. Run the program inside the container with the following command. \
docker start e0b72ff850f8 \
docker exec e0b72ff850f8 python3 firstprogram.py \
where e0b72ff850f8 is the containerID.
6. Save your copied program inside docker image with docker commit. \
docker commit e0b72ff850f8 abc/trydock \
where e0b72ff850f8 is the containerID.
7. Push docker image to the dockerhub.
* Tag image name with different name.\
docker tag abc/trydock username/repo \
where username is your username on dockerhub and repo is the name of image. 
* Push on dockerhub.\
docker push username/repo


